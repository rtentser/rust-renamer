use clap::{command, Arg, Command};

fn main() {
    let matches = command!()
        .arg_required_else_help(true)
        .subcommand(
            Command::new("increment")
                .short_flag('i')
                .about("Rename files in increasing order")
                .arg(
                    Arg::new("names")
                        .short('n')
                        .long("names")
                        .help("Path to text file with names to append to file names"),
                ),
        )
        .subcommand(
            Command::new("remove")
                .short_flag('r')
                .about("Remove characters from file names")
                .arg(
                    Arg::new("length")
                        .short('l')
                        .long("length")
                        .help("How many characters to remove")
                        .required(true),
                )
                .arg(
                    Arg::new("start")
                        .short('s')
                        .long("start")
                        .default_value("0")
                        .help("Where to start in file name (0 by default)"),
                ),
        )
        .arg(
            Arg::new("path")
                .short('p')
                .long("path")
                .help("Path to directory with files to rename"),
        )
        .get_matches();

    match matches.subcommand() {
        Some(("increment", sub_matches)) => {
            println!(
                "increment was used, names are: {:?}",
                sub_matches.get_one::<String>("names")
            )
        }
        _ => unreachable!("Exhausted list of subcommands and subcommand_required prevents `None`"),
    }
}
